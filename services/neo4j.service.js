const neo4j = require("neo4j-driver").v1;

class Neo4jService {
  constructor() {
    this.driver = neo4j.driver(
      "bolt://localhost:7687",
      neo4j.auth.basic("neo4j", "ezdj")
    );
  }

  //check https://github.com/neo4j-examples/movies-javascript-bolt
  session() {
    const session = this.driver.session();

    const personName = "Alice";
    // const resultPromise = session.run(
    //     'CREATE (a:Person {name: $name}) RETURN a',
    //     { name: personName }
    // );
    const resultPromise = session.run("MATCH (n) RETURN n LIMIT 25");

    return resultPromise
      .then((result) => {
        console.log(result);
        return result;
      })
      .finally((_) => {
        session.close();
      });
  }

  runCommand(command, attribute) {
    const session = this.driver.session();
    // const resultPromise = session.run(
    //     'CREATE (a:Person {name: $name}) RETURN a',
    //     { name: personName }
    // );
    const resultPromise = session.run(command, attribute);

    return resultPromise
      .then((result) => {
        // console.log(result);
        return result;
      })
      .finally((_) => {
        session.close();
      });
  }

  closeNeo4j() {
    this.driver.close();
  }
}

const neo4jService = new Neo4jService();
module.exports = neo4jService;
