var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors');
var neo4jService = require('./services/neo4j.service')
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var connectRouter = require('./routes/spotify-connect');
var spotifyRouter = require('./routes/spotify');
var spotifyPlayerRouter = require('./routes/spotify-player');
var channelRouter = require('./routes/channel');

var app = express();

app.use(logger('dev'));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/connect', connectRouter);
app.use('/spotify', spotifyRouter);
app.use('/player', spotifyPlayerRouter);
app.use('/channels', channelRouter);
app.closeNeo4j = () => neo4jService.closeNeo4j();

module.exports = app;
