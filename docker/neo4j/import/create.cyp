
CREATE (Emilien:User {
    spotifyId: "1153169676"
})
CREATE ( Chill: Channel {
    name: "Pour s'ambiancer"
})

CREATE ( Rock: Channel {
    name: "Rock session"
})

CREATE ( Guitarmass: Track {
    spotifyId: "6VHBZt8T7ZdlUt3MkOZqPy"
})
CREATE ( IrishLegend: Track {
    spotifyId: "0G6rdIsquisxkZZSUzLGxq"
})

CREATE (Emilien)-[:CONNECT ]->(Chill)
CREATE (Chill)-[:PLAY {order:1 }]->(Guitarmass)
CREATE (Chill)-[:PLAY {order:2 }]->(IrishLegend)
CREATE (Emilien)-[:CONNECT ]->(Rock)
CREATE (Rock)-[:PLAY {order:1 }]->(IrishLegend)
CREATE (Rock)-[:PLAY {order:2 }]->(Guitarmass)
