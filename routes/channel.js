var express = require("express");
var router = express.Router();
var neo4jService = require("../services/neo4j.service");

router.get("/", function (req, res, next) {
  neo4jService
    .runCommand(
      "MATCH(c:Channel)\
        OPTIONAL MATCH (u:User)-[r:CONNECT]->(c)\
        return ID(c) as id, c.name as name, count(u)+1 as nbUser"
    )
    .then((result) => {
      let record = result.records.map((record) => {
        return {
          channelId: record.get("id").low,
          channelname: record.get("name"),
          nbUser: record.get("nbUser").low,
        };
      });
      // res.status(200).send(result);
      res.status(200).send(record);
    });
});

router.get("/:idChannel", function (req, res, next) {
  let idChannel = parseInt(req.params.idChannel);
  neo4jService
    .runCommand(
      "MATCH(c:Channel)\
          WHERE ID(c)={id}\
          OPTIONAL MATCH (u:User)-[r:CONNECT]->(c)\
        return ID(c) as id, c.name as name, count(u)+1 as nbUser",
      { id: idChannel }
    )
    .then((result) => {
      let record = result.records.map((record) => {
        console.log("record", record);

        return {
          channelId: record.get("id").low,
          channelname: record.get("name"),
          nbUser: record.get("nbUser").low,
        };
      });
      // res.status(200).send(result);
      res.status(200).send(record);
    });
});
router.delete("/:idChannel", function (req, res, next) {
  let idChannel = parseInt(req.params.idChannel);
  if (!idChannel) {
    res.status(422).send("no idTrack or idChannel");
  }
  neo4jService
    .runCommand(
      "MATCH (c:Channel) \
      WHERE id(c)={idChannel} \
      OPTIONAL MATCH (c)<-[r]->() \
      DELETE r,c",
      { idChannel }
    )
    .then((result) => {
      res.status(200).send(result);
    })
    .catch((err) => {
      res.status(422).send(err);
    });
});

router.get("/:idChannel/tracks", function (req, res, next) {
  let idChannel = parseInt(req.params.idChannel);
  neo4jService
    .runCommand(
      "MATCH(c:Channel) \
        WHERE ID(c)={id}\
        MATCH (c)-[r:PLAY]->(t:Track) \
        return t.spotifyId as spotifyId, r.order as order \
        ORDER BY r.order",
      { id: idChannel }
    )
    .then((result) => {
      let record = result.records.map((record) => {
        console.log("record", record);

        return {
          spotifyId: record.get("spotifyId"),
          order: record.get("order"),
        };
      });
      // res.status(200).send(result);
      res.status(200).send(record);
    });
});

router.post("/", function (req, res, next) {
  var name = req.query.name || null;
  var id = req.query.idUser || null;
  console.log("query", req.query);

  if (!name || !id) {
    res.status(400).send();
  }
  neo4jService
    .runCommand(
      "MATCH(u:User{spotifyId:{id}}) \
            CREATE ( c:Channel {name: $name})\
            CREATE (u)-[:OWN ]->(c)\
            RETURN ID(c) as id, c.name as name, 1 as nbUser",
      { name, id }
    )
    .then((result) => {
      let record = result.records.map((record) => {
        console.log("record", record);

        return {
          channelId: record.get("id").low,
          channelname: record.get("name"),
          nbUser: record.get("nbUser").low,
        };
      })[0];
      res.status(200).send(record);
      // }
    })
    .catch((err) => {
      res.status(400).send(err);
    });
});

router.post("/:idChannel/tracks", function (req, res, next) {
  let idChannel = parseInt(req.params.idChannel);
  var idTrack = req.query.idTrack;
  if (!idTrack || !idChannel) {
    res.status(422).send("no idTrack or idChannel");
  }
  let maxOrder = 0;
  // Get max order
  neo4jService
    .runCommand(
      "MATCH(c:Channel) \
            WHERE ID(c)={id} \
            MATCH (c)-[r:PLAY]->(t:Track) \
            RETURN max(r.order) as maxOrder",
      { id: idChannel }
    )
    .then((result) => {
      if (result.records.length > 0) {
        maxOrder = result.records[0].get("maxOrder");
      }
      maxOrder++;
      console.log("max", maxOrder);
      // Get if track exists
      return neo4jService.runCommand(
        "MATCH(t:Track{spotifyId:{id}}) \
            RETURN t",
        { id: idTrack }
      );
    })
    .then((result) => {
      if (result.records.length == 0) {
        return neo4jService.runCommand("CREATE (t:Track{spotifyId:$id})", {
          id: idTrack,
        });
      }
    })
    .then((result) => {
      return neo4jService.runCommand(
        "MATCH(t:Track{spotifyId:{idTrack}}) \
            MATCH(c:Channel) \
            WHERE ID(c)={idChannel} \
            CREATE(c)-[:PLAY{order:$maxOrder}]->(t) \
            RETURN t.spotifyId as spotifyId",
        { idChannel, idTrack, maxOrder }
      );
    })
    .then((result) => {
      // if (result.summary.counters.get('relationshipsCreated') === 0) {
      //     res.status(422).send({ error: 'no nodes created' });
      // } else {
      res.status(200).send({
        spotifyId: idTrack,
        order: maxOrder,
      });
      // }
    })
    .catch((err) => {
      res.status(422).send(err);
    });
});

router.delete("/:idChannel/tracks/:idTrack", function (req, res, next) {
  let idChannel = parseInt(req.params.idChannel);
  var idTrack = req.params.idTrack;
  if (!idTrack || !idChannel) {
    res.status(422).send("no idTrack or idChannel");
  }
  let maxOrder = 0;
  // Get max order
  neo4jService
    .runCommand(
      "MATCH(c:Channel) \
            WHERE ID(c)={idChannel} \
            MATCH (c)-[p:PLAY]->(:Track{spotifyId:{idTrack}}) \
            DELETE p",
      { idChannel, idTrack }
    )
    .then((result) => {
      res.status(200).send(result);
    })
    .catch((err) => {
      res.status(422).send(err);
    });
});

module.exports = router;
