var confSpotify = require("../conf/confSpotify");
var express = require("express");
var router = express.Router();
var request = require("request");
const { toBase64 } = require("request/lib/helpers");

var client_id = confSpotify.clientId; // Your client id
var client_secret = confSpotify.clientSecret; // Your secret
var redirect_uri = confSpotify.redirectUri; // Your redirect uri
var scopes = confSpotify.scopes.join(" "); // Your redirect uri

var stateKey = "spotify_auth_state";

/* GET users listing. */
router.get("/", function (req, res, next) {
  res.status(200).send({ info: "start to connect" });
});

router.get("/login-info", function (req, res) {
  // your application requests authorization
  // res.redirect('https://accounts.spotify.com/authorize?' +
  //     querystring.stringify({
  //         response_type: 'code',
  //         client_id: client_id,
  //         scope: scope,
  //         redirect_uri: redirect_uri,
  //         state: state
  //     }));
  res.status(200).send({
    response_type: "code",
    client_id: client_id,
    scope: scopes,
  });
});

router.get("/callback", function (req, res) {
  // your application requests refresh and access tokens
  // after checking the state parameter

  var code = req.query.code || null;

  res.clearCookie(stateKey);

  var authOptions = {
    url: "https://accounts.spotify.com/api/token",
    form: {
      code: code,
      redirect_uri: redirect_uri,
      grant_type: "authorization_code",
    },
    headers: {
      Authorization:
        "Basic " +
        new Buffer.from(client_id + ":" + client_secret).toString("base64"),
    },
    json: true,
  };
  request.post(authOptions, function (error, response, body) {
    console.log(response.statusCode);
    if (!error && response.statusCode === 200) {
      var access_token = body.access_token,
        refresh_token = body.refresh_token;

      var options = {
        url: "https://api.spotify.com/v1/me",
        headers: { Authorization: "Bearer " + access_token },
        json: true,
      };

      // use the access token to access the Spotify Web API
      request.get(options, function (error, response, body) {
        console.log(body);
      });

      // we can also pass the token to the browser to make requests from there
      res.status(200).send({
        access_token: access_token,
        refresh_token: refresh_token,
      });
    } else {
      res.status(412).send({
        error: "invalid_token",
        desc: response,
        body: body,
      });
    }
  });
});

router.get("/refresh_token", function (req, res) {
  // requesting access token from refresh token
  var refresh_token = req.query.refresh_token;
  console.log("refresh", refresh_token);
  var authOptions = {
    url: "https://accounts.spotify.com/api/token",
    headers: {
      Authorization: "Basic " + toBase64(client_id + ":" + client_secret),
    },
    form: {
      grant_type: "refresh_token",
      refresh_token: refresh_token,
    },
    json: true,
  };

  request.post(authOptions, function (error, response, body) {
    if (!error && response.statusCode === 200) {
      var access_token = body.access_token;
      res.send({
        access_token: access_token,
      });
    }
  });
});

module.exports = router;
