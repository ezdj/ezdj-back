var express = require('express');
var router = express.Router();
var request = require('request');
var querystring = require('querystring');


/* GET users listing. */
router.get('/', function (req, res, next) {
    res.status(200).send({ info: 'start to do some spotify api' });
});

router.get('/play/track', function (req, res, next) {
    var access_token = req.query.access_token || null;
    var player_id = req.query.player_id || null;
    var spotify_uri = req.query.spotify_uri || null;
    console.log('spotify_uri', spotify_uri);
    
    var options = {
        url: 'https://api.spotify.com/v1/me/player/play',
        headers: { 'Authorization': 'Bearer ' + access_token },
        json: true,
        qs: {device_id: player_id},
        body: { uris: JSON.parse(spotify_uri) }
    };

    console.log(options);
    
    // use the access token to access the Spotify Web API
    request.put(options, function (error, response, body) {
        res.status(200).send({ body });
    });
});

router.get('/play/context', function (req, res, next) {
    var access_token = req.query.access_token || null;
    var player_id = req.query.player_id || null;
    var spotify_uri = req.query.spotify_uri || null;
    var options = {
        url: 'https://api.spotify.com/v1/me/player/play',
        headers: { 'Authorization': 'Bearer ' + access_token },
        json: true,
        qs: {device_id: player_id},
        body: { context_uri: spotify_uri }
    };

    // use the access token to access the Spotify Web API
    request.put(options, function (error, response, body) {
        res.status(200).send({ body });
    });
});

module.exports = router;