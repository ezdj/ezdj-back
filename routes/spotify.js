var express = require('express');
var router = express.Router();
var request = require('request');
var querystring = require('querystring');


/* GET users listing. */
router.get('/', function (req, res, next) {
    res.status(200).send({ info: 'start to do some spotify api' });
});

router.get('/me', function (req, res, next) {
    var access_token = req.query.access_token || null;
    var options = {
        url: 'https://api.spotify.com/v1/me',
        headers: { 'Authorization': 'Bearer ' + access_token },
        json: true
    };

    // use the access token to access the Spotify Web API
    request.get(options, function (error, response, body) {
        res.status(200).send(body);
    });
});

router.get('/search', function (req, res, next) {
    var access_token = req.query.access_token || null;
    var q = req.query.q || null;
    var limit = parseInt(req.query.limit) || 5;
    search(q, 'artist,track', limit, access_token, (body) => {
        console.log(body);
        
        res.status(200).send({artists: body.artists ? body.artists.items : [], tracks: body.tracks ? body.tracks.items : []});
    });
});

router.get('/search/track', function (req, res, next) {
    var access_token = req.query.access_token || null;
    var q = req.query.q || null;
    var limit = parseInt(req.query.limit) || 5;
    search(q, 'track', limit, access_token, (body) => {
        res.status(200).send(body.tracks ? body.tracks.items : []);
    });
});

router.get('/search/artist', function (req, res, next) {
    var access_token = req.query.access_token || null;
    var q = req.query.q || null;
    var limit = parseInt(req.query.limit) || 5;
    search(q, 'artist', limit, access_token, (body) => {
        res.status(200).send(body.artists ? body.artists.items : []);
    });
});

router.get('/tracks/:id', function(req, res, next) {
    var access_token = req.query.access_token || null;
    let idTrack = req.params.id;
    console.log('id', idTrack);
    
    var options = {
        url: `https://api.spotify.com/v1/tracks/${idTrack}`,
        headers: { 'Authorization': 'Bearer ' + access_token },
        json: true
    };

    // use the access token to access the Spotify Web API
    request.get(options, function (error, response, body) {
        res.status(200).send(body);
    });
    
});

router.get('/tracks/:id/audio-features', function(req, res, next) {
    var access_token = req.query.access_token || null;
    let idTrack = req.params.id;
    console.log('id', idTrack);
    
    var options = {
        url: `https://api.spotify.com/v1/audio-features/${idTrack}`,
        headers: { 'Authorization': 'Bearer ' + access_token },
        json: true
    };

    // use the access token to access the Spotify Web API
    request.get(options, function (error, response, body) {
        res.status(200).send(body);
    });
    
});

function search(q, type, limit, access_token, func) {
    var options = {
        url: 'https://api.spotify.com/v1/search?' + 
        querystring.stringify({
            q,
            type,
            market: 'from_token',
            limit: limit,
            offset: 0,
            include_external: 'audio'
        }),
        headers: { 'Authorization': 'Bearer ' + access_token },
        json: true
    };

    // use the access token to access the Spotify Web API
    request.get(options, function (error, response, body) {
        func(body);
    });
}

module.exports = router;